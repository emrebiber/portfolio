﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_DVDLibrary.MODELS;

namespace MVC_DVDLibrary.UI.Models
{
    public class MovieViewModel
    {
        public Movie Movie { get; set; }
        public List<SelectListItem> MovieItems { get; set; }


        public MovieViewModel()
        {
            MovieItems = new List<SelectListItem>();
        }

        public void SetMovieItems(IEnumerable<Movie> movies)
        {
            foreach (var movie in movies)
            {
                MovieItems.Add(new SelectListItem()
                {
                    Value = movie.DVDId.ToString(),
                    Text = movie.Title
                });
            }
        }
    }

    
}