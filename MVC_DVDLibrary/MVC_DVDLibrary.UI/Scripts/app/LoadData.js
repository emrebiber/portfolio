﻿
var url = '/api/Movie/';

$(document)
    .ready(function() {
        loadMovies();
    });

function loadMovies() {
    $.getJSON(url)
        .done(function(data) {
            $('#MovieTable tbody tr').remove();
            $.each(data,
                function(index, movie) {
                    $(createRow(movie)).appendTo($('#MovieTable tbody'));
                });
        });
    };

function createRow(movie) {
        return '<tr><td>' +
            movie.DVDId +
            '</td><td>' +
            movie.Title +
            '</td><td>' +
            movie.ReleaseDate +
            '</td><td>' +
            movie.MpaaRating +
            '</td><td>' +
            movie.DirectorName +
            '</td><td>' +
            movie.Studio +
            '</td><td>' +
            movie.UserRating +
            '</td><td>' +
            movie.UserNotes +
            '</td><td>' +
            movie.ActorsInMovie +
            '</td></tr>';
}




