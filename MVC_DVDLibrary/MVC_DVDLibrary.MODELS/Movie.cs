﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace MVC_DVDLibrary.MODELS
{
    public class Movie
    { 
        public int DVDId { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string MpaaRating { get; set; }
        public string DirectorName { get; set; }
        public string Studio { get; set; }
        public decimal UserRating { get; set; }
        public string UserNotes { get; set; }
        public string[] ActorsInMovie { get; set; }


    }
}
