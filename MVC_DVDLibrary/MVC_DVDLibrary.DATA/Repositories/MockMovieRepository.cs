﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVDLibrary.DATA.Interfaces;
using MVC_DVDLibrary.MODELS;

namespace MVC_DVDLibrary.DATA.Repositories
{
    public class MockMovieRepository : IMovieRepository
    {
        private static List<Movie> _movies;

        static MockMovieRepository()
        {
            _movies = new List<Movie>()
            {
                new Movie()
                {
                    DVDId = 1,
                    Title = "Mission Impossible",
                    ReleaseDate = DateTime.Parse("06/25/2000"),
                    MpaaRating= "G",
                    DirectorName = "Christopher McQuarrie",
                    Studio = "Paramount Pictures",
                    UserRating = 9.0M,
                    UserNotes = "Best Movie Ever",
                    ActorsInMovie = new[] {"Tom Cruise", "Jeremy Renner", "Rebecca Ferguson"},
                },

                new Movie()
                {
                    DVDId = 2,
                    Title = "Spiderman",
                    ReleaseDate = DateTime.Parse("05/03/2002"),
                    MpaaRating = "PG-13",
                    DirectorName = "Stan Lee",
                    Studio = "Columbia Pcitures",
                    UserRating = 7.3M,
                    UserNotes = "Average Movie",
                    ActorsInMovie = new[] {"Tobey Maguire", "Willem Dafoe", "Kirsten Dunst", "James Franco"},

                },

                 new Movie()
                {
                    DVDId = 3,
                    Title = "The Dark Knight",
                    ReleaseDate = DateTime.Parse("11/23/2008"),
                    MpaaRating = "PG-13",
                    DirectorName = "Christopher Nolan",
                    Studio = "Columbia Pcitures",
                    UserRating = 9.9M,
                    UserNotes = "Best Movie I have ever watched whole my life!!!",
                    ActorsInMovie = new[] {"Christian Bale", "Heath Ledger", "Aaron Eckhart", "Michael Kane","Maggie Gyllenhaal"},

                },

                  new Movie()
                {
                    DVDId = 4,
                    Title = "Star Wars: Episode VII - The Force Awakens",
                    ReleaseDate =DateTime.Parse("08/17/2015"),
                    MpaaRating = "R",
                    DirectorName = "J.J. Abrams",
                    Studio = "Warner Bros Studios",
                    UserRating = 5.3M,
                    UserNotes = "It was terrible movie. I will go back to original series.",
                    ActorsInMovie = new[] {"Tobey Maguire", "Willem Dafoe", "Kirsten Dunst", "James Franco"},

                },

              
            };
        }

        public List<Movie> List()
        {
            return _movies;
        }

        public Movie GetOneSpecificMovie(int dvdId)
        {
            return _movies.FirstOrDefault(m => m.DVDId == dvdId);
        }

        public Movie GetOneSpecificMovieByTitle(string title)
        {
            return _movies.FirstOrDefault(m => m.Title == title);
        }

        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }

        public void Edit(Movie movie)
        {
            var selectedMovie = _movies.First(m => m.DVDId == movie.DVDId);

            selectedMovie.Title = movie.Title;
            selectedMovie.ReleaseDate = movie.ReleaseDate;
            selectedMovie.MpaaRating = movie.MpaaRating;
            selectedMovie.DirectorName = movie.DirectorName;
            selectedMovie.Studio = movie.Studio;
            selectedMovie.UserRating = movie.UserRating;
            selectedMovie.UserNotes = movie.UserNotes;
            selectedMovie.ActorsInMovie = movie.ActorsInMovie;
        }

        public void Delete(int dvdId)
        {
            _movies.RemoveAll(m => m.DVDId == dvdId);
        }

        public void Search(string title)
        {
            var movie = from m in _movies
                where m.Title == title
                select m;

            //var movie = _movies.Where(m => m.Title == title);
            
        }
    }
}
