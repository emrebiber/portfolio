﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVDLibrary.DATA.Interfaces;
using MVC_DVDLibrary.DATA.Repositories;

namespace MVC_DVDLibrary.DATA.Factories
{
    public static class MovieRepositoryFactory
    {
        public static IMovieRepository GetRightRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new MockMovieRepository();
                default:
                    throw new ArgumentException("This error comes from MovieRepo Factory.");
            }
        }
    }
}
